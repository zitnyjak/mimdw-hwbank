/**
 * BankAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package client;

public interface BankAccount extends java.rmi.Remote {
    public boolean validateAccount(java.lang.String arg0) throws java.rmi.RemoteException;
    public double pay(java.lang.String arg0, double arg1) throws java.rmi.RemoteException;
    public boolean validateBalance(java.lang.String arg0, double arg1) throws java.rmi.RemoteException;
}
