package example;

import client.BankAccount;
import client.BankAccountServiceLocator;

/**
 * Created by d_rc on 23/11/14.
 */
public class HelloWorldClient {
  public static void main(String[] argv) {
      try {
          BankAccountServiceLocator locator = new BankAccountServiceLocator();
          BankAccount service = locator.getBankAccountPort();
          String x = "x";
          if (service.validateAccount("XY123789d23")){
            x = "ok";
          } else {
            x = "nok";
          }
          System.out.println(x);
      } catch (javax.xml.rpc.ServiceException ex) {
          ex.printStackTrace();
      } catch (java.rmi.RemoteException ex) {
          ex.printStackTrace();
      }
  }
}
