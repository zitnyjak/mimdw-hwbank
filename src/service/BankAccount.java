package service;

import model.AccountDB;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Bank Account WS
 * @author Jakub Zitny <zitnyjak@fit.cvut.cz>
 * @since Sun Nov 23 21:16:10 CET 2014
 */
@WebService()
public class BankAccount {

    AccountDB db = AccountDB.getInstance();

    @WebMethod
    public boolean validateAccount (String accNumber) {
        return db.exists(accNumber);
    }

    @WebMethod
    public boolean validateBalance (String accNumber, double amount) {
        if (db.getAccount(accNumber).getBalance() >= amount) {
            return true;
        }
        return false;
    }

    @WebMethod
    public double pay (String accNumber, double amount) {
        return db.getAccount(accNumber).pay(amount);
    }

}
