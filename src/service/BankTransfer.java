package service;

import client.BankAccountServiceLocator;
import model.AccountDB;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Bank Transfer WS
 * @author Jakub Zitny <zitnyjak@fit.cvut.cz>
 * @since Sun Nov 23 21:16:10 CET 2014
 */
@WebService()
public class BankTransfer {

    AccountDB db = AccountDB.getInstance();

    @WebMethod
    public boolean transfer (String from, String to, double amount) {
        try {
            BankAccountServiceLocator locator = new BankAccountServiceLocator();
            client.BankAccount service = locator.getBankAccountPort();
            if (!service.validateAccount(from)) return false;
            if (!service.validateAccount(to)) return false;
            db.getAccount(from).pay(-1 * amount);
            db.getAccount(to).pay(amount);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
