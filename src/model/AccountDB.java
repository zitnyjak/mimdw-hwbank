package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by d_rc on 23/11/14.
 */
public class AccountDB {

    private static AccountDB mInstance;
    private List<Account> mAccountCollection = new ArrayList<Account>();

    public static AccountDB getInstance() {
        if (mInstance == null) {
            mInstance = new AccountDB();
        }
        return mInstance;
    }

    public AccountDB () {
        mAccountCollection.add(new Account("S123456789"));
        mAccountCollection.add(new Account("XY12378923"));
        mAccountCollection.add(new Account("ASDV123467", 100));
    }

    public void addAccount (Account acc) {
        mAccountCollection.add(acc);
    }

    public Account getAccount (String accNumber) {
        for (Account searchedAcc: mAccountCollection) {
            if (searchedAcc.getAccNumber().equals(accNumber))
                return searchedAcc;
        }
        return null;
    }

    public boolean exists (String accNumber) {
        for (Account searchedAcc: mAccountCollection) {
            if (searchedAcc.getAccNumber().equals(accNumber))
                return true;
        }
        return false;
    }
}
