package model;

/**
 * Created by d_rc on 23/11/14.
 */
public class Account {

    private static double INITBALANCE = 0.0;
    private String mAccNumber;
    private double mBalance;

    public Account (String accNumber) {
        setAccNumber(accNumber);
        setBalance(INITBALANCE);
    }

    public Account (String accNumber, double balance) {
        setAccNumber(accNumber);
        setBalance(balance);
    }

    public String getAccNumber() {
        return mAccNumber;
    }

    public void setAccNumber(String accNumber) {
        this.mAccNumber = accNumber;
    }

    public double getBalance() {
        return mBalance;
    }

    public void setBalance (double balance) {
        this.mBalance = balance;
    }

    public double pay (double amount) {
        mBalance += amount;
        return mBalance;
    }

}
